<<<<<<< HEAD
![Image Description](https://eazytraining.fr/wp-content/uploads/2020/08/CI_CD_pipeline.jpg)

**Gitlab-ci : Construire un Pipeline Complet avec Heroku, Docker**

Ce dépôt est utilisé pour la formation [gitlab-ci](https://eazytraining.fr/cours/gitlab-ci-cd-pour-devops/) proposée par [eazytraining](https://eazytraining.fr/). 
Les étapes du pipeline se divisent en deux parties principales :

1. **CI (Intégration Continue)**
Cette étape fournit les actions nécessaires pour construire notre artefact :
- **Build** : Construire l'image Docker basée sur l'[application web](https://github.com/eazytrainingfr/alpinehelloworld.git).
- **Test d'Acceptation** : Pour valider l'application conteneurisée.
- **Release/Package** : Étiqueter et pousser l'image/la release sur un registre privé.

2. **CD (Déploiement Continu)**
Pour le déploiement, nous avons utilisé le fournisseur de cloud Heroku pour héberger notre application conteneurisée ! C'est très facile à utiliser : avec la CLI Heroku, nous utilisons un token API pour automatiser la provision de l'environnement.
- **Review** : Déployer l'application pour révision après la création d'une PR par les développeurs. Nous utilisons les [Environnements Dynamiques](https://docs.gitlab.com/ee/ci/environments/#configuring-dynamic-environments) de Gitlab-ci.
- **Staging** : Déployer dans un environnement de staging pour valider l'application avant sa mise en ligne.
- **Production** : Mettre l'application en ligne.

C'était un plaisir pour nous de vous enseigner le [DevOps](https://eazytraining.fr/parcours-devops/) avec des outils formidables comme Gitlab, Heroku, Docker...

=======
# AlpineHelloWorld

## Pour Commencer

Pour vous faciliter la prise en main de GitLab, voici une liste d'étapes recommandées pour bien débuter.

Déjà un pro ? Modifiez simplement ce fichier README.md et personnalisez-le. Vous voulez simplifier les choses ? [Utilisez le modèle en bas de page](#modifier-ce-readme) !

## Ajoutez vos fichiers

- [ ] [Créez](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) ou [téléversez](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) des fichiers
- [ ] [Ajoutez des fichiers en utilisant la ligne de commande](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) ou poussez un dépôt Git existant avec la commande suivante :

```
cd existing_repo
git remote add origin https://gitlab.com/SHoward225/alpinehelloworld.git
git branch -M main
git push -uf origin main
```

## Intégrez avec vos outils

- [ ] [Configurez les intégrations de projet](https://gitlab.com/SHoward225/alpinehelloworld/-/settings/integrations)

## Collaborez avec votre équipe

- [ ] [Invitez des membres de l'équipe et des collaborateurs](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Créez une nouvelle demande de fusion (merge request)](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Fermez automatiquement les problèmes (issues) à partir des demandes de fusion](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Activez les approbations de demande de fusion](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Configurez l'auto-fusion](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Utilisez l'intégration continue intégrée dans GitLab.

- [ ] [Commencez avec GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analysez votre code pour détecter les vulnérabilités connues avec le Test de Sécurité des Applications Statiques (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Déployez sur Kubernetes, Amazon EC2 ou Amazon ECS en utilisant le déploiement automatique](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Utilisez les déploiements basés sur les requêtes pour une meilleure gestion de Kubernetes](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Configurez des environnements protégés](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Modifier ce README

Lorsque vous êtes prêt à personnaliser ce README, éditez simplement ce fichier et utilisez le modèle pratique ci-dessous (ou structurez-le comme vous le souhaitez - ceci n'est qu'un point de départ !). Merci à [makeareadme.com](https://www.makeareadme.com/) pour ce modèle.

## Suggestions pour un bon README

Chaque projet est différent, donc considérez quelles sections s'appliquent au vôtre. Les sections utilisées dans le modèle sont des suggestions pour la plupart des projets open source. Gardez également à l'esprit qu'un README peut être trop long et détaillé, mais il vaut mieux qu'il soit trop long que trop court. Si vous pensez que votre README est trop long, envisagez d'utiliser une autre forme de documentation plutôt que de supprimer des informations.

## Nom
Choisissez un nom explicite pour votre projet.

## Description
Informez les gens sur ce que votre projet peut faire spécifiquement. Fournissez du contexte et ajoutez un lien vers toute référence que les visiteurs pourraient ne pas connaître. Une liste de fonctionnalités ou une sous-section sur le contexte peut également être ajoutée ici. Si des alternatives à votre projet existent, c'est un bon endroit pour lister les facteurs de différenciation.

## Badges
Sur certains READMEs, vous pouvez voir de petites images qui transmettent des métadonnées, telles que si tous les tests passent pour le projet. Vous pouvez utiliser Shields pour en ajouter à votre README. De nombreux services ont également des instructions pour ajouter un badge.

## Visuels
Selon ce que vous créez, il peut être une bonne idée d'inclure des captures d'écran ou même une vidéo (vous verrez souvent des GIF plutôt que de véritables vidéos). Des outils comme ttygif peuvent aider, mais consultez Asciinema pour une méthode plus sophistiquée.

## Installation
Dans un écosystème particulier, il peut y avoir une manière commune d'installer les choses, comme utiliser Yarn, NuGet ou Homebrew. Cependant, considérez que la personne qui lit votre README pourrait être novice et aimerait plus de conseils. Lister des étapes spécifiques aide à éliminer l'ambiguïté et permet aux gens d'utiliser votre projet le plus rapidement possible. S'il ne fonctionne que dans un contexte spécifique comme une version particulière de langage de programmation ou un système d'exploitation ou a des dépendances qui doivent être installées manuellement, ajoutez également une sous-section Exigences.

## Utilisation
Utilisez des exemples libéralement et montrez le résultat attendu si vous le pouvez. Il est utile d'avoir en ligne le plus petit exemple d'utilisation que vous pouvez démontrer, tout en fournissant des liens vers des exemples plus sophistiqués s'ils sont trop longs pour être raisonnablement inclus dans le README.

## Support
Dites aux gens où ils peuvent aller pour obtenir de l'aide. Cela peut être une combinaison d'un suivi des problèmes, d'une salle de chat, d'une adresse e-mail, etc.

## Feuille de route
Si vous avez des idées pour des sorties futures, il est judicieux de les lister dans le README.

## Contribuer
Indiquez si vous êtes ouvert aux contributions et quelles sont vos exigences pour les accepter.

Pour les personnes qui souhaitent apporter des modifications à votre projet, il est utile d'avoir une documentation sur la manière de commencer. Peut-être y a-t-il un script qu'ils doivent exécuter ou des variables d'environnement qu'ils doivent définir. Rendez ces étapes explicites. Ces instructions pourraient également être utiles pour votre futur vous.

Vous pouvez également documenter des commandes pour lint le code ou exécuter des tests. Ces étapes aident à garantir une haute qualité du code et à réduire la probabilité que les changements cassent involontairement quelque chose. Avoir des instructions pour exécuter des tests est particulièrement utile s'il nécessite une configuration externe, comme démarrer un serveur Selenium pour tester dans un navigateur.

## Auteurs et remerciements
Montrez votre appréciation à ceux qui ont contribué au projet.

## Licence
Pour les projets open source, dites comment il est licencié.

## Statut du projet
Si vous avez manqué d'énergie ou de temps pour votre projet, mettez une note en haut du README disant que le développement a ralenti ou s'est complètement arrêté. Quelqu'un peut choisir de forker votre projet ou de se porter volontaire pour intervenir en tant que mainteneur ou propriétaire, permettant à votre projet de continuer. Vous pouvez également faire une demande explicite de mainteneurs.

>>>>>>> 8de4fd16f9065d7d2abbc4bd829d75a4d4ef94e4
